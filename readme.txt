Bubble sort

https://www.youtube.com/watch?v=tFxA9v4gRpE
https://www.youtube.com/watch?v=oqpICiM165I
https://www.youtube.com/watch?v=nmhjrI-aW5o

https://www.geeksforgeeks.org/bubble-sort/


Selection sort

https://www.youtube.com/watch?v=RAj-CG3bgqk
https://www.youtube.com/watch?v=8Y89DRq9Y3o
https://www.youtube.com/watch?v=xWBP4lzkoyM

https://www.geeksforgeeks.org/selection-sort/


Insertion sort

https://www.youtube.com/watch?v=z_iFuwvmQgA
https://www.youtube.com/watch?v=dAAkElskMmU
https://www.youtube.com/watch?v=OGzPmgsI-pQ
https://www.youtube.com/watch?v=lCDZ0IprFw4

http://www.java2novice.com/java-sorting-algorithms/insertion-sort/


Binary insertion sort

https://www.youtube.com/watch?v=lxesF43JP8U


Binary search

https://www.youtube.com/watch?v=xgrxUijL8PY
https://www.youtube.com/watch?v=R_aZdj4vtFI


Quick sort (Tony Hoare's sort)

https://www.youtube.com/watch?v=kq_EM_7Wdt8

https://www.youtube.com/watch?v=Xgaj0Vxz_to
https://www.youtube.com/watch?v=PgBzjlCcFvc

https://www.youtube.com/watch?v=4s-aG6yGGLU

https://www.geeksforgeeks.org/quick-sort/

Merge sort

https://www.youtube.com/watch?v=wQMSowIS2FY

