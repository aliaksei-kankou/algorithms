package sort;

public class QuickSort {
    public static void main(String[] args) {
        Util.display(sortAsc(Data.arr.clone(), 0, Data.arr.length - 1));
    }

    private static int[] sortAsc(int[] arr, int l, int r) {
        if (l < r) {
            int pi = partition(arr, l, r);
            sortAsc(arr, l, pi - 1);
            sortAsc(arr, pi, r);
        }
        return arr;
    }

    private static int partition(int[] arr, int l, int r) {
        int pivot = arr[r];
        int wall = l - 1;
        for (int i = l; i < r; i++) {
            if (arr[i] < pivot) {
                wall++;
                Util.swap(arr, i, wall);
            }
        }
        wall++;
        Util.swap(arr, r, wall);
        return wall;
    }
}
