package sort;

import java.util.Arrays;

public class BinaryInsertionSort {
    public static void main(String[] args) {
        int[] sortedArray = sortAsc(Data.arr.clone());
        Util.display(sortedArray);
        Util.display(binarySortAsc(sortedArray, 12));
    }

    private static int[] binarySortAsc(int[] arr, int e) {
        int n = arr.length;
        int[] newArr = Arrays.copyOf(arr, n + 1);
        int appropriateIndex = binarySearch(arr, n, e);
        System.arraycopy(newArr, appropriateIndex, newArr, appropriateIndex + 1, n - appropriateIndex);
        newArr[appropriateIndex] = e;
        return newArr;
    }

    public static int[] sortAsc(int[] arr) {
        int n = arr.length;
        for (int i = 1; i < n; i++) {
            int appropriateIndex = binarySearch(arr, i, arr[i]);
            if (appropriateIndex < i) {
                for (int j = i; j > appropriateIndex; j--) {
                    int t = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = t;
                }
            }
        }
        return arr;
    }

    private static int binarySearch(int[] arr, int r, int e) {
        int l = -1;
        while (l < r - 1) {
            int m = (l + r) / 2;
            if (arr[m] >= e) {
                r = m;
            } else {
                l = m;
            }
        }
        return r;
    }
}
