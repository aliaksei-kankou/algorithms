package sort;

public class MergeSort {
    public static void main(String[] args) {
        Util.display(sortAsc(Data.arr.clone(), 0, Data.arr.length - 1));
    }

    public static int[] sortAsc(int[] arr, int l, int r) {
        if (l < r) {
            int m = (l + r) / 2;
            sortAsc(arr, l , m);
            sortAsc(arr, m + 1 , r);
            merge(arr, l, m, r);
        }
        return arr;
    }

    public static void merge(int[] arr, int l, int m, int r) {
        int l1 = m - l + 1;
        int l2 = r - m;

        int[] arr1 = new int[l1];
        int[] arr2 = new int[l2];

        for (int i = 0; i < l1 ; i++) {
            arr1[i] = arr[l + i];
        }

        for (int i = 0; i < l2 ; i++) {
            arr2[i] = arr[m + 1 + i];
        }
        int i = 0, j = 0, k = l;
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                arr[k++] = arr1[i++];
            } else {
                arr[k++] = arr2[j++];
            }
        }
        while (i < arr1.length) {
            arr[k++] = arr1[i++];
        }
        while (j < arr2.length) {
            arr[k++] = arr2[j++];
        }
    }
}
