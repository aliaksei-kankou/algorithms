package sort;

public class SelectionSort {
    public static void main(String[] args) {
        Util.display(sortAscMaxLeft(Data.arr.clone()));
        Util.display(sortAscMaxRight(Data.arr.clone()));
        Util.display(sortAscMinLeft(Data.arr.clone()));
        Util.display(sortAscMinRight(Data.arr.clone()));

        Util.display(sortDescMaxLeft(Data.arr.clone()));
        Util.display(sortDescMaxRight(Data.arr.clone()));
        Util.display(sortDescMinLeft(Data.arr.clone()));
        Util.display(sortDescMinRight(Data.arr.clone()));


    }

    public static int[] sortAscMaxLeft(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int maxInd = 0;
            for (int j = 1; j < n - i; j++) {
                if (arr[j] > arr[maxInd]) {
                    maxInd = j;
                }
            }
            if (maxInd < n - i) {
                Util.swap(arr, maxInd, n - i - 1);
            }
        }
        return arr;
    }

    public static int[] sortAscMaxRight(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int maxInd = n - i - 1;
            for (int j = n - i - 1; j >= 0; j--) {
                if (arr[j] > arr[maxInd]) {
                    maxInd = j;
                }
            }
            if (maxInd < n - i - 1) {
                Util.swap(arr, maxInd, n - i - 1);
            }
        }
        return arr;
    }

    public static int[] sortAscMinLeft(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int minInd = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[minInd]) {
                    minInd = j;
                }
            }
            if (minInd > i) {
                Util.swap(arr, minInd, i);
            }
        }
        return arr;
    }

    public static int[] sortAscMinRight(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int minInd = i;
            for (int j = n - 1; j >= i; j--) {
                if (arr[j] < arr[minInd]) {
                    minInd = j;
                }
            }
            if (minInd > i) {
                Util.swap(arr, minInd, i);
            }
        }
        return arr;
    }

    public static int[] sortDescMaxLeft(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int maxInd = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] > arr[maxInd]) {
                    maxInd = j;
                }
            }
            if (maxInd > i) {
                Util.swap(arr, i, maxInd);
            }
        }
        return arr;
    }

    public static int[] sortDescMaxRight(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int maxInd = i;
            for (int j = n - 1; j > i; j--) {
                if (arr[j] > arr[maxInd]) {
                    maxInd = j;
                }
            }
            if (maxInd > i) {
                Util.swap(arr, i, maxInd);
            }
        }

        return arr;
    }

    public static int[] sortDescMinLeft(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int minInd = 0;
            for (int j = 1; j < n - i; j++) {
                if (arr[j] < arr[minInd]) {
                    minInd = j;
                }
            }
            if (minInd < n - i) {
                Util.swap(arr, n - i - 1, minInd);
            }
        }
        return arr;
    }

    public static int[] sortDescMinRight(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int minInd = n - i - 1;
            for (int j = n - i - 2; j >=0; j--) {
                if (arr[j] < arr[minInd]) {
                    minInd = j;
                }
            }
            if (minInd < n - i - 1) {
                Util.swap(arr, n - i - 1, minInd);
            }
        }
        return arr;
    }
}
