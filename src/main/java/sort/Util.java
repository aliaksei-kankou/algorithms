package sort;

import java.util.Arrays;

public class Util {

    //public static void swap(int[] arr, int i) {
    //    int t = arr[i - 1];
    //    arr[i - 1] = arr[i];
    //    arr[i] = t;
    //}

    public static void swap(int[] arr, int index1, int index2) {
        int t = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = t;
    }

    public static void display(int[] arr) {
        System.out.println(Arrays.toString(arr));
    }
}
