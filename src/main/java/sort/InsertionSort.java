package sort;

public class InsertionSort {
    public static void main(String[] args) {
        Util.display(sortAsc(Data.arr.clone()));
    }

    private static int[] sortAsc(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int el = arr[i + 1];
            int j;
            for (j = i; j >= 0 && el < arr[j]; j--) {
               arr[j + 1] = arr[j];
            }
            if (j + 1 < i)
            arr[j + 1] = el;
        }
        return arr;
    }

}
