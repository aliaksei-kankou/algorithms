package sort;

public class BubbleSort {

    public static void main(String[] args) {
        Util.display(sortAscLeft(Data.arr.clone()));
        Util.display(sortAscRight(Data.arr.clone()));
        Util.display(sortDescLeft(Data.arr.clone()));
        Util.display(sortDescRight(Data.arr.clone()));
    }

    public static int[] sortAscLeft(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            boolean swapped = false;
            for (int j = 1; j < n - i; j++) {
                if (arr[j - 1] > arr[j]) {
                    Util.swap(arr, j, j - 1);
                    swapped = true;
                }
            }
            if (!swapped) break;
        }
        return arr;
    }

    public static int[] sortAscRight(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = n - 1; j > i; j--) {
                if (arr[j] < arr[j - 1]) {
                    Util.swap(arr, j, j - 1);
                }
            }
        }
        return arr;
    }

    public static int[] sortDescLeft(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 1; j < n - i; j++) {
                if (arr[j - 1] < arr[j]) {
                    Util.swap(arr, j, j - 1);
                }
            }
        }
        return arr;
    }

    public static int[] sortDescRight(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = n - 1; j > i; j--) {
                if (arr[j] > arr[j - 1]) {
                    Util.swap(arr, j, j - 1);
                }
            }
        }
        return arr;
    }

}
