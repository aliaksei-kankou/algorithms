package search;

public class LeftBinarySearch {
    public static void main(String[] args) {
        int x = 7;
        Integer[] arr = {
                new Integer(1), new Integer(2), new Integer(5), new Integer(7), new Integer(7), new Integer(7),
                new Integer(7), new Integer(9), new Integer(10), new Integer(15),
        };

        Integer found = search(arr, x);
    }

    public static Integer search(Integer[] arr, int x) {
        int left = 0, right = arr.length;

        while (left < right - 1) {
            int mid = (left + right) / 2;
            if (arr[mid] >= x) {
                right = mid;
            } else {
                left = mid;
            }
        }

        if (arr[right] == x) {
            return arr[right];
        } else {
            return null;
        }
    }
}
