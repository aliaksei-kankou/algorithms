package structure;

import java.util.Arrays;

public class MinHeap {
    private int DEFAULT_CAPACITY = 5;
    private int size;
    private int capacity;
    private int[] data;

    public MinHeap() {
        capacity = DEFAULT_CAPACITY;
        data = new int[capacity];
    }

    public MinHeap(int capacity) {
        this.capacity = capacity;
        data = new int[this.capacity];
    }

    public void add(int x) {
        ensureCapacity();
        if (size == 0) {
            data[size++] = x;
        }
        else {
            data[size++] = x;
            heapifyUp();
        }
    }

    public int poll() {
        if (size == 0) {
            throw new IllegalArgumentException();
        }
        int e;
        if (size == 1) {
            size--;
            e = data[0];
        }
        else {
            e = data[0];
            data[0] = data[size-- - 1];
            heapifyDown();
        }
        return e;
    }

    private void heapifyDown() {
        int i = 0;
        while(hasLeftChild(i)) {
            int smallerChildIndex = getLeftChildIndex(i);
            if (hasRightChild(i) && data[getRightChildIndex(i)] < data[getLeftChildIndex(i)]) {
                smallerChildIndex = getRightChildIndex(i);
            }
            if (data[i] < data[smallerChildIndex]) {
                break;
            } else {
                swap(i, smallerChildIndex);
                i = smallerChildIndex;
            }

        }

    }

    private boolean hasRightChild(int i) {
        return getRightChildIndex(i) < size;
    }

    private int getRightChildIndex(int i) {
        return (i << 1) + 2;
    }

    private boolean hasLeftChild(int i) {
        return getLeftChildIndex(i) < size;
    }

    private int getLeftChildIndex(int i) {
        return (i << 1) + 1;
    }

    private void ensureCapacity() {
        if (size == capacity) grow();
    }

    private void grow() {
        capacity += capacity >>> 1;
        data = Arrays.copyOf(data, capacity);
    }

    private void heapifyUp() {
        int elementIndex = size - 1;
        while (hasParent(elementIndex)) {
            int parentIndex = getParentIndex(elementIndex);
            if (data[elementIndex] < data[parentIndex]) {
                swap(elementIndex, parentIndex);
                elementIndex = parentIndex;
            } else {
                break;
            }
        }
    }

    private void swap(int elementIndex, int parentIndex) {
        int temp = data[elementIndex];
        data[elementIndex] = data[parentIndex];
        data[parentIndex] = temp;
    }

    private int getParentIndex(int elementIndex) {
        return elementIndex - 1 >>> 1;
    }

    private boolean hasParent(int elementIndex) {
        return elementIndex != 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public static void main(String[] args) {
        MinHeap heap = new MinHeap();

        heap.add(10);
        heap.add(9);
        heap.add(8);
        heap.add(7);
        heap.add(6);
        heap.add(5);
        heap.add(4);
        heap.add(3);
        heap.add(2);
        heap.add(1);

        while (!heap.isEmpty()) {
            System.out.println(heap.poll());
        }

        
    }
}
