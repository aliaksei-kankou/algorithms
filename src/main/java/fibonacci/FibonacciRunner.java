package fibonacci;

public class FibonacciRunner {
    public static void main(String[] args) {
        var fibonacci = new Fibonacci();
        System.out.println(fibonacci.plainFib(40));
        System.out.println(fibonacci.optimalFib(100));
        System.out.println(fibonacci.cachedFib(100));
    }
}
