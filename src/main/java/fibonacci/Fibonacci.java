package fibonacci;

public class Fibonacci {
    // T(N) = O(2ˆN)
    long plainFib(int n) {
        if (n <= 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return plainFib(n - 1) + plainFib(n - 2);
    }

    // T(N) = O(N)
    long optimalFib(int n) {
        var values = new long[n + 1];
        values[0] = 0;
        values[1] = 1;
        for (int i = 2; i <= n; i++) {
            values[i] = values[i - 1] + values[i - 2];
        }
        return values[n];
    }

    // T(N) = O(N)
    long cachedFib(int n) {
        return cachedFib(n, new long[n + 1]);
    }

    private long cachedFib(int n, long[] cache) {
        if (n <= 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        if (cache[n] == 0) {
            cache[n] = cachedFib(n - 1, cache) + cachedFib(n - 2, cache);
        }
        return cache[n];
    }
}
